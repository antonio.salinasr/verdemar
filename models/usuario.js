import mongoose, { Schema, model, models } from 'mongoose';


const UsuarioSchema = new Schema({
  usuario :                 {type: String, required: [true, 'El campo usuario es requerido.'],  },
  password :                {type: String, required: [true, 'El campo password es requerido.'],  },
  inicio :                  {type: String, required: [true, 'El campo inicio es requerido.'],  },
  termino :                 {type: String, required: [true, 'El campo termino es requerido.'],  },
  rol :                     {type: String, required: [true, 'El campo rol es requerido.'],  },
  activo :                  {type: String, required: [true, 'El campo activo es requerido.'],  },
 
});

// const Alumno = mongoose.model('Alumno', AlumnoSchema);
export const Usuario = mongoose.models.Usuario || mongoose.model('Usuario', UsuarioSchema);

export default Usuario;

