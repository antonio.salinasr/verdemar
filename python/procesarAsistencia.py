import xlrd
import pymongo
from insertarAsistencia import insertAsistencia
def buscarAsignatura(asignatura):
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["cft"]
    mycol = mydb["asignaturas"]
    buscar = mycol.find({"codigo":asignatura})
    if(len(list(buscar)) != 0):
        # print('entontrado')
        return True
    return(False)


def main():
   try:
        book = xlrd.open_workbook("data/InformeAsistencia.xls")
        print("The number of worksheets is {0}".format(book.nsheets))
        print("Worksheet name(s): {0}".format(book.sheet_names()))
        sh = book.sheet_by_index(0)
        asignatura = ''
        anno = ''
        for rx in range(sh.nrows):
            # print (sh.cell_value(rowx=rowRamo, colx=colRamo))
            # resp = buscarAsignatura(sh.cell_value(rowx=rx, colx=colRamo))
            # rowRamo+=1
            if(buscarAsignatura(sh.cell_value(rowx=rx, colx=5))):
                asignatura= sh.cell_value(rowx=rx, colx=5)
                anno = str(int(sh.cell_value(rowx=rx+2, colx=10)))
                seccion = str(int(sh.cell_value(rowx=rx+2, colx=5)))
                periodo = str(int(sh.cell_value(rowx=rx+2, colx=14)))
                regimen = str(int(sh.cell_value(rowx=rx+2, colx=19)))

            if(sh.cell_value(rowx=rx, colx=0) != '' and sh.cell_value(rowx=rx, colx=0) != 'RUT'):  
                asistencia = {}
                rut_entero = sh.cell_value(rowx=rx, colx=0).split()  
                rut = rut_entero[0]
                dv = rut_entero[2]
                nombre_alumno = sh.cell_value(rowx=rx, colx=2)
                hrs_teoricas = str(int(sh.cell_value(rowx=rx, colx=7)))
                hrs_total_planificadas = str(int(sh.cell_value(rowx=rx, colx=11)))
                hrs_dictadas = str(int(sh.cell_value(rowx=rx, colx=15)))
                hrs_asistidas = str(int(sh.cell_value(rowx=rx, colx=20)))
                hrs_justificadas = sh.cell_value(rowx=rx, colx=21)
                
                porcent_asistencia = sh.cell_value(rowx=rx, colx=23)
                # print(asignatura, 
                #       anno, 
                #       seccion, 
                #       periodo,regimen,rut,dv, nombre_alumno,hrs_teoricas,hrs_total_planificadas,
                #       hrs_dictadas,hrs_asistidas,hrs_justificadas,porcent_asistencia)
            
                asistencia["asignatura"] = asignatura
                asistencia["anno"] = anno
                asistencia["seccion"] = seccion
                asistencia["periodo"] = periodo
                asistencia["regimen"] = regimen
                asistencia["rut"] = rut
                asistencia["dv"] = dv
                asistencia["nombre_alumno"] = nombre_alumno
                asistencia["hrs_teoricas"] = hrs_teoricas
                asistencia["hrs_total_planificadas"] = hrs_total_planificadas
                asistencia["hrs_dictadas"] = hrs_dictadas
                asistencia["hrs_asistidas"] = hrs_asistidas
                asistencia["hrs_justificadas"] = hrs_justificadas
                asistencia["porcent_asistencia"] = porcent_asistencia
                insertAsistencia(asistencia)
   except:
       print("no encontrado")

__main__ = main()