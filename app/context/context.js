
'use client';

import React,{ createContext, useState } from "react";

export const MyContext = createContext()

export function MyContextProvider({children}){
    const [ctxShowUserNew,setCtxShowUserNew] = useState(false)
    const [ctxShowUserEdit,setCtxShowUserEdit] = useState(false)
    const setShowUserNew = (show) =>{
        setCtxShowUserNew(show)
    }
    const setShowUserEdit = (show) =>{
        setCtxShowUserEdit(show)
    }
    return(
        <MyContext.Provider value = {{ctxShowUserNew,setCtxShowUserNew,ctxShowUserEdit,setCtxShowUserEdit}}>
            {children}
        </MyContext.Provider>
    )
}
