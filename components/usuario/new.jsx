"use client"
import React from 'react'
import { useState,useContext } from 'react'
import { MyContext } from '../../app/context/context'


function newUsuario() {

const [usuario,setUsuario] = useState({
    usuario:"",
    password:"",
    inicio:"",
    termino:"",
    rol:"",
})
const {ctxShowUserNew,setCtxShowUserNew} = useContext(MyContext)


const hadleChange = (e) =>{

        const newUser = {...usuario,...{[e.target.id] : e.target.value }}
        setUsuario(newUser)
        console.log(usuario)
}
const handleClick = async () =>{
    const response = await fetch("/api/usuario/new",{
        method: "POST",
        body : JSON.stringify(usuario)
    })
    if(response.status == 200){
        alert("Guardado exitosamente")
        setCtxShowUserNew(false)
    }else{
        alert("problemas al guardar")
    }
}
  return (
    <div className="container mx-auto">
        <div className="mt-8 max-w-md">
            <div className="grid grid-cols-1 gap-6">
            <label className="block">
                <span className="text-gray-700">Usuario</span>
                <input type="text" className="mt-1 block w-full" placeholder="Usuario" id="usuario"  onChange={(e) => hadleChange(e)}/>
            </label>
            <label className="block">
                <span className="text-gray-700">Password</span>
                <input type="password" className="mt-1 block w-full" placeholder="Password" id="password"  onChange={(e) => hadleChange(e)}/>
            </label>
            <label className="block">
                <span className="text-gray-700">Inicio</span>
                <input type="date" className="mt-1 block w-full"  id="inicio" onChange={(e) => hadleChange(e)}/>
            </label>
            <label className="block">
                <span className="text-gray-700">Termino</span>
                <input type="date" className="mt-1 block w-full" id="termino"  onChange={(e) => hadleChange(e)}/>
            </label>
            <label className="block">
                <span className="text-gray-700">Rol</span>
                <select className="block w-full mt-1" id = "rol" onChange={(e) => hadleChange(e)}>
                    <option value="1">Usuario</option>
                    <option value="2">Rol 2</option>
                    <option value="3">Rol 3</option>
                    <option value="4">Administrador</option>
                </select>
            </label>
            <button onClick ={handleClick} className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow formButton">
                Guardar
            </button>      
            </div>
        </div>
    </div>
  )
}

export default newUsuario