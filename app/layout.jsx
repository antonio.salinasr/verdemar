import "../styles/globals.css";
import Nav from "../components/Nav";
import { MyContextProvider } from "./context/context";

export const metadata = {
  title: "Verdemar",
  description: "Sistema de Gestion",
};

const RootLayout = ({ children }) => (
  <html lang='en'>
    <body>

        <div className='main'>
          <div className='gradient' />
        </div>
        <main className='app'>
        <MyContextProvider>
        <Nav/>
           {children}
        </MyContextProvider>
          
        </main>

    </body>
  </html>
);

export default RootLayout;
