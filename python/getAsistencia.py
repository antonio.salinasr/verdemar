from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.service import Service
from fake_useragent import UserAgent
import time
import datetime
from selenium.webdriver.support.ui import Select

def getAsistencia(anno, periodo,finicio, ffin, usr, psw):
        opts = Options()
        ua = UserAgent()
        user_agent = ua.firefox
        opts.add_argument(user_agent)
        print(user_agent)
        service = Service(executable_path=r".\geckodriver.exe") 
        driver = webdriver.Firefox(service=service)
    # try:
        user = usr
        password = psw
        url = 'https://cev.umas.cl/umasnet/Home2.aspx'
        driver.get(url=url)
        time.sleep(2)
        inputUser = driver.find_element(By.ID, "cons_user")
        inputPass = driver.find_element(By.ID,"cons_clave")
        select = driver.find_element(By.ID, "cbo_perfile")
        button = driver.find_element(By.ID, "Button2")
        inputUser.send_keys(user)
        inputPass.send_keys(password)
        select.click()
        time.sleep(2)
        button.click()
        time.sleep(10)
        buttonInf = driver.find_element(By.CLASS_NAME,"ux-startbutton-center")
        buttonInf.click()
        extgen125 = driver.find_element(By.ID,"ext-gen125")
        time.sleep(5)
        extgen125.click()
        extgen180 = driver.find_element(By.ID,"ext-gen180")
        time.sleep(3)
        extgen180.click()
        extgen215 = driver.find_element(By.ID,"ext-gen215")
        time.sleep(3)
        extgen215.click()
        time.sleep(2)
        iframe = driver.find_element(By.ID, "test")
        driver.switch_to.frame(iframe)
        today = datetime.date.today()
        year = today.year
        print(year)
        addOpt = """
            const addOpt = () =>{
                txt_ANO = document.getElementById('txt_ANO')
                txt_ANO.value = 2023
                txt_FECHAINI = document.getElementById('txt_FECHAINI')
                txt_FECHAINI.value = '01-01-2023'
                txt_FECHAFIN = document.getElementById('txt_FECHAFIN')
                txt_FECHAFIN.value = '01-12-2023'
                
            }
            addOpt()
        """
        driver.execute_script(addOpt)
        txt_PERIODO = driver.find_element(By.ID,"txt_PERIODO")
        txt_PERIODO.send_keys(periodo)
        btnGenerar = driver.find_element(By.ID, "btnGenerar")
        btnGenerar.click()
        time.sleep(5)
        driver.switch_to.default_content()
        iframe = driver.find_elements(By.TAG_NAME,"iframe")[8]
        driver.switch_to.frame(iframe)
        select = Select(driver.find_element(By.ID,'ReportViewer1_ctl01_ctl05_ctl00'))
        select.select_by_visible_text("Excel")
        time.sleep(5)
        time.sleep(5)
        excel = driver.find_element(By.ID,'ReportViewer1_ctl01_ctl05_ctl01')
        excel.click()
        time.sleep(30)
        driver.close()
    # except:
    #     driver.close()
    
__main__ = getAsistencia('2023','1','01-09-2023','31-12-2023','145502934','145502934')