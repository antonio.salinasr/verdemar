import Usuario from "../../../../models/usuario";
import { connectToDB } from "../../../../utils/database";

export const POST = async (request) => {

    try {
        await connectToDB();
        const resultados = await Usuario.find({})
        return new Response(JSON.stringify(resultados), { status: 200 })
    } catch (error) {
        return new Response(`Falló al obtener usuarios ${error}`, { status: 500 });
    }
}