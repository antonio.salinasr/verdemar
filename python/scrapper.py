from bs4 import BeautifulSoup as bs 
import requests 

from alumnos import getAlumnos
from parserAlumno import parseAlummo
from notas import getNotas
from parserNotas import parseNotas
from insertAlumno import insertAlumno    
from insertNota import insertNota
from getcarreras import getCarreras
from getAsistencia import getAsistencia
def main():
    URL = "https://cev.umas.cl/umasnet/Home2.aspx" 
    payload = { 

"__VIEWSTATE": "/wEPDwUJMTczMzUyNTkzD2QWAmYPZBYGAgUPZBYEAgUPFgIeB1Zpc2libGVoFgICAw9kFgJmD2QWAgIFDxBkZBYAZAIHD2QWAgICD2QWAmYPZBYCAgEPZBYCAgMPEGRkFCsBAWZkAgcPD2QWAh4Kb25rZXlwcmVzcwVNaWYgKGV2ZW50LmtleUNvZGU9PTEzKXtkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndHh0X2NsYXZlUmVpbmdyZXNvJykuZm9jdXMoKX1kAgkPD2QWAh8BBUVpZiAoZXZlbnQua2V5Q29kZT09MTMpe2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdidG5fZ3JhYmFyJykuZm9jdXMoKX1kGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYCBQdCdXR0b24yBQdMaW1waWFyyGY8xqp1vC/3nWcEJeplC+ie7mc=",
"__VIEWSTATEGENERATOR": "EE2ECF30",
"cons_user": "145502934", 
"cons_clave": "67d2a17a8f0969fa5ae89f7b8ec886f35a5ddb21",
"cbo_perfile": "254",
"cbo_clientes": "1",
"hidden_cliente": "1",
"hidden_perfil": "254",
"Button2.x": "39",
"Button2.y": "23",

    } 
    s = requests.session() 
    response = s.post(URL, data=payload) 
    print(response.status_code) 
    carreras = getCarreras()
    for carrera in carreras:
        data = getAlumnos(s,carrera["codigo"])
        allData = parseAlummo(data)
        for alumno in allData:
            insertAlumno(alumno)
        for x in range(2020,2024,1):    
            data = getNotas(s,x,carrera["codigo"],'V')
            allData = parseNotas(data)
            for nota in allData:
                insertNota(nota)
            data = getNotas(s,x,carrera["codigo"],'D')
            for nota in allData:
                insertNota(nota)
            allData = parseNotas(data)
    # allData = parseNotas(data)
    # for nota in allData:
    #     insertNota(nota)
            
    #     # data = getNotas(s,'VM',x)
    #     # allData = parseNotas(data)
    #     # for nota in allData:
    #     #     insertNota(nota)
            
    #     # data = getNotas(s,'LA',x)
    #     # allData = parseNotas(data)
    #     # for nota in allData:
    #     #     insertNota(nota)
    

    
__main__ = main()
