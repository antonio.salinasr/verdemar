from html.parser import HTMLParser
from bs4 import BeautifulSoup
        
def parseAlummo(toParse):
    # print(toParse)
    soup = BeautifulSoup(str(toParse), 'html.parser')
    datos = soup.find_all('font')
    count = 0
    alumnos=[]
    for dato in datos:
        # print(dato)
        if(count == 0):
            alumno = {}
            alumno['numero'] = dato.text
        if(count == 1):
            alumno['ano'] = dato.text
        if(count == 2):
            alumno['periodo'] = dato.text
        if(count == 3):
            alumno['codcli'] = dato.text
        if(count == 4):
            alumno['rut'] = dato.text
        if(count == 5):
            alumno['dig'] = dato.text
        if(count == 6):
            alumno['paterno'] = dato.text
        if(count == 7):
            alumno['materno'] = dato.text
        if(count == 8):
            alumno['nombres'] = dato.text
        if(count == 9):
            spl = str(dato.text).split('-')
            if(len(spl)==3):
                alumno['fecha_nac'] = ("{}-{}-{}").format(spl[2],spl[0],spl[1])
        if(count == 10):
            alumno['diractual'] = dato.text
        if(count == 11):
            alumno['comuna'] = dato.text
        if(count == 12):
            alumno['ciudadact'] = dato.text
        if(count == 13):
            if(dato.text == 'M'):
                alumno['sexo'] = 'MASCULINO'
            elif(dato.text == 'F'): 
                alumno['sexo'] = 'FEMENINO'
            else:
                alumno['sexo'] = 'OTRO NO BINARIO'
                
 
        if(count == 14):
            alumno['codcarr'] = dato.text
        if(count == 15):
            alumno['nombre_carrera'] = dato.text
        if(count == 16):
            if(dato.text == "D"):
                alumno['jornada'] = 'DIURNO'
            else:
                alumno['jornada'] = 'VESPERTINO'
                
        if(count == 17):
            alumno['estacad'] = dato.text 
        if(count == 18):
            alumno['fono1'] = dato.text
        if(count == 19):
            alumno['fono2'] = dato.text
        if(count == 20):
            alumno['mail_personal'] = dato.text
        if(count == 21):
            alumno['mail_institucional'] = dato.text
        if(count == 22):
            alumno['cae'] = dato.text
        if(count == 23):
            alumno['glosa_cae'] = dato.text
        if(count == 24):
            alumno['beca_nm'] = dato.text
        if(count == 25):
            alumno['beca_excelencia_tecnica'] = dato.text
        if(count == 26):
            alumno['otra_beca'] = dato.text
        if(count == 27):
            alumno['notaem'] = dato.text 
        if(count == 28):
            alumno['ano_mat'] = dato.text
        if(count == 29):
            alumno['periodo_mat'] = dato.text
            alumnos.append(alumno)
            count = 0
        else:
            # print(count,dato.text)
            count+=1
    return(alumnos)
        # print(alumno) 
    # print(alumnos)   

# __main__: parse()
