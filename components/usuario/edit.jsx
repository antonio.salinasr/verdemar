"use client"
import React from 'react'
import { useState,useEffect,useContext } from 'react'
import { MyContext } from '../../app/context/context'


function editUsuario({user}) {

const [usr,setUsr] = useState({})
const {ctxShowUserEdit,setCtxShowUserEdit} = useContext(MyContext) 

useEffect(()=>{
    {
        console.log(user)
        setUsr(user)

    }
    return () =>{
        console.log(usr)
        console.log('unmount')
    }
},[])
const hadleChange = (e) =>{

        const newUser = {...usr,...{[e.target.id] : e.target.value }}
        setUsr(newUser)
        console.log(usr)
}
const handleClick = async () =>{
    const response = await fetch("/api/usuario/edit",{
        method: "POST",
        body : JSON.stringify(usr)
    })
    if(response.status == 200){
        alert("Guardado exitosamente")
        setCtxShowUserEdit(false)
    }else{
        alert("Problemas al guardar")
    }
}
  return (
    <div className="container mx-auto">
        <div className="mt-8 max-w-md">
            <div className="grid grid-cols-1 gap-6">
            <label className="block">
                <span className="text-gray-700">Usuario</span>
                <input value={usr.usuario} type="text" className="mt-1 block w-full" placeholder="Usuario" id="usuario"  onChange={(e) => hadleChange(e)}/>
            </label>
            <label className="block">
                <span className="text-gray-700">Password</span>
                <input value ={usr.password} type="password" className="mt-1 block w-full" placeholder="Password" id="password"  onChange={(e) => hadleChange(e)}/>
            </label>
            <label className="block">
                <span className="text-gray-700">Inicio</span>
                <input value ={usr.inicio} type="date" className="mt-1 block w-full"  id="inicio" onChange={(e) => hadleChange(e)}/>
            </label>
            <label className="block">
                <span className="text-gray-700">Termino</span>
                <input value = {usr.termino} type="date" className="mt-1 block w-full" id="termino"  onChange={(e) => hadleChange(e)}/>
            </label>
            <label className="block">
                <span className="text-gray-700">Rol</span>
                <select value={usr.rol} className="block w-full mt-1" id = "rol" onChange={(e) => hadleChange(e)}>
                    <option value="1">Usuario</option>
                    <option value="2">Rol 2</option>
                    <option value="3">Rol 3</option>
                    <option value="4">Administrador</option>
                </select>
            </label>
            <label className="block">
                <span className="text-gray-700">Activo</span>
                <select value={usr.activo} className="block w-full mt-1" id = "activo" onChange={(e) => hadleChange(e)}>
                    <option value="true">SI</option>
                    <option value="false">NO</option>
                </select>
            </label>
            <button onClick ={handleClick} className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow formButton">
                Guardar
            </button>
            </div>
        </div>
    </div>
  )
}

export default editUsuario