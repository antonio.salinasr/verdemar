import pymongo
def insertAsistencia(asistencia):
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["cft"]
    mycol = mydb["asistencia"]
    buscar = mycol.find({
         "asignatura": asistencia["asignatura"],
         "anno": asistencia["anno"], 
         "seccion":asistencia["seccion"], 
         "periodo":asistencia["periodo"],
         "regimen":asistencia["regimen"],
         "rut":asistencia["rut"]
        })
    if(len(list(buscar)) == 0):
        mycol.insert_one(asistencia)
        print('Asistencia Insertado')
    else:
        query = {
                "asignatura": asistencia["asignatura"],
                "anno": asistencia["anno"], 
                "seccion":asistencia["seccion"], 
                "periodo":asistencia["periodo"],
                "regimen":asistencia["regimen"],
                "rut":asistencia["rut"]
                 }
        update = {"$set":{
            
                "dv" : asistencia["dv"],
                "nombre_alumno": asistencia["nombre_alumno"],
                "hrs_teoricas":asistencia["hrs_teoricas"],
                "hrs_total_planificadas": asistencia["hrs_total_planificadas"],
                "hrs_dictadas":asistencia["hrs_dictadas"],
                "hrs_asistidas":asistencia["hrs_asistidas"],
                "hrs_justificadas":asistencia["hrs_justificadas"],
                "porcent_asistencia":asistencia["porcent_asistencia"],
        }}
        mycol.update_one(query,update)
        print('Asistencia Ya Existe, Actualizado')


