import pymongo
def insertNota(nota):
    try:
        myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        mydb = myclient["cft"]
        # alumnos = mydb["alumnos"]
        # alumno = alumnos.find_one({"rut":nota['rut']})
        # print("id->",alumno['_id'])
        mycol = mydb["calificaciones"]
        buscar = mycol.find({
            # 'alumnos':alumno["_id"],
            'ano': nota['ano'] , 
            'periodo': nota['periodo'] , 
            'codcli': nota['codcli'] , 
            'rut': nota['rut'] , 
            'ano_ingreso': nota['ano_ingreso'] , 
            'carrera_alum': nota['carrera_alum'] , 
            'jornada': nota['jornada'] , 
            'nivel': nota['nivel'] , 
            'cod_curso': nota['cod_curso'] , 
            'seccion': nota['seccion'] , 
            'num_nota': nota['num_nota'] , 
            })
        if(len(list(buscar)) == 0):
            mycol.insert_one(nota)
            print('calificacion Insertada')
        else:
            query = {"rut":nota["rut"]}
            update = {"$set":{
                        # 'alumnos':alumno["_id"],
                        # 'alumnos_id':alumno["_id"],
                        'numero':nota['numero'] ,
                        'ano':nota['ano'],
                        'periodo':nota['periodo'] ,
                        'codcli':nota['codcli'] ,
                        'rut':nota['rut'] ,
                        'nombre_alumno':nota['nombre_alumno']  , 
                        'ano_ingreso':nota['ano_ingreso'] ,
                        'cat_alumno':nota['cat_alumno'] ,
                        'estado_alumno':nota['estado_alumno'],
                        'cod_sede':nota['cod_sede'] ,
                        'nombre_sede':nota['nombre_sede'], 
                        'carrera_alum':nota['carrera_alum'] ,
                        'nombre_carrera_alumno':nota['nombre_carrera_alumno'] ,
                        'regimen_carrera_alumno':nota['regimen_carrera_alumno'], 
                        'codcarr_planificada':nota['codcarr_planificada'], 
                        'nombre_carrera_planificada':nota['nombre_carrera_planificada'] , 
                        'jornada':nota['jornada'] ,
                        'rut_prof':nota['rut_prof'], 
                        'nombre_prof':nota['nombre_prof']  , 
                        'nivel':nota['nivel'] ,
                        'cod_curso':nota['cod_curso'], 
                        'nombre_curso':nota['nombre_curso'] , 
                        'seccion':nota['seccion'] ,
                        'actividad':nota['actividad'], 
                        'num_nota':nota['num_nota'] ,
                        'nota_parcial':nota['nota_parcial'], 
                        'nota_final_curso':nota['nota_final_curso'],
                        
                        
            }}
            mycol.update_one(query,update)
            # alumnos.update_one({"rut":nota['rut']},{})
            print('Calificacion Ya Existe, Actualizado')
    except:
        print('alumno no encontrado')

