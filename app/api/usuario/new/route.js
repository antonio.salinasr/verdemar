import Usuario from "../../../../models/usuario";
import { connectToDB } from "../../../../utils/database";

export const POST = async (request) => {

    const {usuario, password, inicio, termino, rol } = await request.json();
    console.log(request.json())
    try {
        await connectToDB();
        const activo = true 
        const newUsuario = new Usuario({ usuario, password, inicio, termino, rol,activo:true});
        await newUsuario.save();
        return new Response(JSON.stringify(newUsuario), { status: 200 })
    } catch (error) {
        return new Response(`Falló al crear usuario ${error}`, { status: 500 });
    }
}