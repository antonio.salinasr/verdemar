"use client"
import React from 'react'
import { useState, useEffect, useContext } from 'react'
import NuevoUsuario from "../../components/usuario/new"
import EditUsuario from "../../components/usuario/edit"
import { Modal } from 'flowbite-react';
import { MyContext } from '../context/context'
import { get } from 'mongoose'

function page() {

    const {ctxShowUserNew,setCtxShowUserNew} = useContext(MyContext)
    const {ctxShowUserEdit,setCtxShowUserEdit} = useContext(MyContext) 
    const [usuarios,setUsuarios] = useState([])
    const [usrTosend,setusrToSend] = useState({})


    
    useEffect(()=>{
        async function getUsers(){
            // setUsuarios([])

            await fetch("/api/usuario/all",{
                method: "POST",
            })
            .then(response => response.json())
            .then(data => setUsuarios(data))
        }
        getUsers()
        setCtxShowUserNew(false)
        setCtxShowUserEdit(false)
        return () =>{
            console.log('MOUNTED')
        }
    },[])
    

    const handleModalEdit = async (usr) =>{
    //   setUsuarios({foo:"bar"})
      setusrToSend(usr)
      console.log(usrTosend)
      setCtxShowUserEdit(true)
    //   getUsers()
      
    }
    return (
        <div className="md:container md:mx-auto">
                <div className="relative overflow-x-auto">
                <div className=" mx-auto bg-white p-6 rounded-md shadow-md" style={{ width:"100%"}}>
                <button onClick={() => setCtxShowUserNew(true)} className="block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" type="button">
                  Nuevo
                </button>
                </div>
                <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" className="px-6 py-3">
                                Usuario
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Inicio
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Termino
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Rol
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Activo
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Acciones
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    {usuarios.map(usr =>(
                        <tr key={usr._id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {usr.usuario}
                            </th>
                            <td className="px-6 py-4">
                                {usr.inicio}
                            </td>
                            <td className="px-6 py-4">
                                {usr.termino}
                            </td>
                            <td className="px-6 py-4">
                                {usr.rol}
                            </td>
                            <td className="px-6 py-4">
                                {usr.activo}
                            </td>
                            <td className="px-6 py-4">
                            <button onClick={() => handleModalEdit(usr)}  type="button" className="focus:outline-none text-white bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:focus:ring-yellow-900">Editar</button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
            <Modal id="new" show={ctxShowUserNew} onClose={() => setCtxShowUserNew(false)}>
              <Modal.Header>Nuevo Usuario</Modal.Header>
              <Modal.Body>
                <NuevoUsuario/>
              </Modal.Body>
            </Modal>

            <Modal id="edit" show={ctxShowUserEdit}  onClose={() => setCtxShowUserEdit(false)}>
              <Modal.Header>Editar Usuario</Modal.Header>
              <Modal.Body>
                <EditUsuario user={usrTosend}/>
              </Modal.Body>
            </Modal>
            
        </div>
    

      
    )
}

export default page