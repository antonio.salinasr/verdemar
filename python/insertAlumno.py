import pymongo
def insertAlumno(alumno):
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["cft"]
    mycol = mydb["alumnos"]
    buscar = mycol.find({"rut":alumno["rut"]})
    if(len(list(buscar)) == 0):
        mycol.insert_one(alumno)
        print('Alumno Insertado')
    else:
        query = {"rut":alumno["rut"]}
        update = {"$set":{
            
            "numero" : alumno['numero'],
            "ano" : alumno['ano'],
            "periodo" : alumno['periodo'],
            "codcli" : alumno['codcli'],
            "rut" : alumno['rut'],
            "dig" : alumno['dig'],
            "paterno" : alumno['paterno'],
            "materno" : alumno['materno'],
            "nombres" : alumno['nombres'],
            "fecha_nac" : alumno['fecha_nac'],
            "diractual" : alumno['diractual'],
            "comuna" : alumno['comuna'],
            "ciudadact" : alumno['ciudadact'],
            "sexo" : alumno['sexo'],
            "codcarr" : alumno['codcarr'],
            "nombre_carrera" : alumno['nombre_carrera'],
            "jornada" : alumno['jornada'],
            "estacad" : alumno['estacad'],
            "fono1" : alumno['fono1'],
            "fono2" : alumno['fono2'],
            "mail_personal" : alumno['mail_personal'],
            "mail_institucional" : alumno['mail_institucional'],
            "cae" : alumno['cae'],
            "glosa_cae" : alumno['glosa_cae'],
            "beca_nm" : alumno['beca_nm'],
            "beca_excelencia_tecnica" : alumno['beca_excelencia_tecnica'],
            "otra_beca" : alumno['otra_beca'],
            "notaem" : alumno['notaem'],
            "ano_mat" : alumno['ano_mat'],
            "periodo_mat" : alumno['periodo_mat'],
        }}
        mycol.update_one(query,update)
        print('Alumno Ya Existe, Actualizado')


