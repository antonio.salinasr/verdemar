import mongoose from 'mongoose';

let isConnected = false; 
const uri = `mongodb://${process.env.MONGODB_USR}:${process.env.MONGODB_PSW}@${process.env.MONGODB_URI}/verdemar?authSource=admin`
export const connectToDB = async () => {
  mongoose.set('strictQuery', true);

  if(isConnected) {
    console.log('MongoDB is already connected');
    return;
  }

  try {
    await mongoose.connect(uri, { 
      dbName: process.env.DBNAME,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })

    isConnected = true;

    console.log('MongoDB connected')
  } catch (error) {
    console.log(error);
  }
}