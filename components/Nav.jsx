"use client";

import Image from "next/image";
import { useState } from "react"

// style="position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate3d(351.2px, 179.2px, 0px);"
// style="position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate3d(263.2px, 179.2px, 0px);"
const Nav = () => {
  const [dropdownOpen, setDropdownOpen] = useState(["hidden","translate3d(0)"]);
  const toggleDropdown = (e) => {
    console.log(e.clientX)
    console.log(e.clientY)
    if(dropdownOpen[0] == "hidden" ){
      setDropdownOpen(["block",`translate3d(${e.clientX-100}px,${e.clientY+15}px,10px)`]);
    }else{
      setDropdownOpen(["hidden",`translate3d(${e.clientX-100}px,${e.clientY+15}px,10px)`]);
    }
    console.log(dropdownOpen)
  }
  return (
   

<nav className="bg-white border-gray-200 dark:bg-gray-900 dark:border-gray-700 bg-gradient-to-r from-indigo-500 from-10% via-sky-500 via-30% to-emerald-500 to-90% to 100%">
  <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
    <a href="#" className="flex items-center space-x-3 rtl:space-x-reverse">
        <img src="https://64.media.tumblr.com/tumblr_m5p9r8zjpT1ruhjceo1_500.jpg" className="h-10" alt="Flowbite Logo" />
        <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">VerdeMar</span>
    </a>
    <button data-collapse-toggle="navbar-dropdown" type="button" className="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="navbar-dropdown" aria-expanded="false">
        <span className="sr-only">Open main menu</span>
        <svg className="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
            <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 1h15M1 7h15M1 13h15"/>
        </svg>
    </button>
    <div className="hidden w-full md:block md:w-auto" id="navbar-dropdown">
      <ul className="flex flex-col font-medium p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:space-x-8 rtl:space-x-reverse md:flex-row md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
      
        <li>
            <button id="dropdownNavbarLink" data-dropdown-toggle="dropdownNavbar" 
            className="flex items-center justify-between w-full py-2 px-3 text-gray-900 rounded
             hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto
              dark:text-white md:dark:hover:text-blue-500 dark:focus:text-white dark:border-gray-700
               dark:hover:bg-gray-700 md:dark:hover:bg-transparent">Mantenedores <svg className="w-2.5 h-2.5 ms-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
    <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 1 4 4 4-4" onClick={(e) => toggleDropdown(e)}/>
  </svg></button>
            {/* <!-- Dropdown menu --> */}
            <div id="dropdownNavbar" 
            className={`z-10 font-normal bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700
             dark:divide-gray-600 ${dropdownOpen[0]}`} style={{position: "absolute", inset: "0px auto auto 0px", margin: "0px",
              transform: dropdownOpen[1]}} data-popper-placement="bottom">
                <ul className="py-2 text-sm text-gray-700 dark:text-gray-400" aria-labelledby="dropdownLargeButton">
                  <li>
                    <a href="/chofer" className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Choferes</a>
                  </li>
                  <li>
                    <a href="/bus" className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Buses</a>
                  </li>
                  <li>
                    <a href="/empresa" className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Empresas</a>
                  </li>
                  <li>
                    <a href="/usuario" className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Usuarios</a>
                  </li>
                </ul>
                <div className="py-1">
                  <a href="#" className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white">Sign out</a>
                </div>
            </div>
        </li>
        
      </ul>
    </div>
  </div>
</nav>

  );
};

export default Nav;