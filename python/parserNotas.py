from html.parser import HTMLParser
from bs4 import BeautifulSoup
import time
def parseNotas(toParse):
    soup = BeautifulSoup(str(toParse), 'html.parser')
    datos = soup.find_all('td')
    count = 0
    notas=[]
    for dato in datos:
        # print (count,dato.text.lower())
        # count +=1
        # time.sleep(2)
        if(count == 0):nota = {}      
        if(count == 0): nota['numero'] = dato.text  
        if(count == 1): nota['ano'] = dato.text
        if(count == 2): nota['periodo'] = dato.text
        if(count == 3): nota['codcli'] = dato.text
        if(count == 4): nota['rut'] = dato.text
        if(count == 5): nota['nombre_alumno']= dato.text 
        if(count == 6): nota['ano_ingreso']= dato.text
        if(count == 7): nota['cat_alumno']= dato.text
        if(count == 8): nota['estado_alumno']= dato.text
        if(count == 9): nota['cod_sede']= dato.text
        if(count == 10): nota['nombre_sede']= dato.text
        if(count == 11): nota['carrera_alum']= dato.text
        if(count == 12): nota['nombre_carrera_alumno']= dato.text
        if(count == 13): nota['regimen_carrera_alumno']= dato.text     
        if(count == 14): nota['codcarr_planificada']= dato.text     
        if(count == 15): nota['nombre_carrera_planificada']= dato.text     
        if(count == 16): nota['jornada']= dato.text     
        if(count == 17): nota['rut_prof']= dato.text     
        if(count == 18): nota['nombre_prof']= dato.text     
        if(count == 19): nota['nivel']= dato.text     
        if(count == 20): nota['cod_curso']= dato.text     
        if(count == 21): nota['nombre_curso']= dato.text     
        if(count == 22): nota['seccion']= dato.text     
        if(count == 23): nota['actividad']= dato.text     
        if(count == 24): nota['num_nota']= dato.text     
        if(count == 25): nota['nota_parcial']= dato.text     
        if(count == 26): 
            nota['nota_final_curso']= dato.text      
            count =0
            notas.append(nota)
            print(nota)
            # time.sleep(1)
        else:
            count+=1
            
            
        
      
    return(notas)
    