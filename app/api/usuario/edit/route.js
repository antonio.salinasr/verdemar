import Usuario from "../../../../models/usuario";
import { connectToDB } from "../../../../utils/database";

export const POST = async (request) => {

    const {usuario, password, inicio, termino, rol, _id, created_at, activo } = await request.json();
    console.log(request.json())
    try {
        await connectToDB();
        const existingUsuario = await Usuario.findById(_id);
        console.log(existingUsuario)
        if (!existingUsuario) {
            return new Response("Usuario no Encontrado", { status: 404 });
        }

        
        existingUsuario.usuario = usuario
        existingUsuario.password = password
        existingUsuario.inicio = inicio
        existingUsuario.termino = termino
        existingUsuario.rol = rol
        existingUsuario.created_at = created_at
        existingUsuario.activo = activo


        await existingUsuario.save();
        return new Response(JSON.stringify(existingUsuario), { status: 200 })

    } catch (error) {
        return new Response(`Falló al editar usuario ${error}`, { status: 500 });
    }
}